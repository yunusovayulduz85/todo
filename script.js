const data = document.querySelector("#data");
const data2 = document.querySelector("#data2")
const time = document.getElementById("time");
const now = new Date();
const today = now.getDate() < 10 ? '0' + now.getDate() : now.getDate();
const month1 = now.getMonth() + 1;
const month = month1 < 10 ? '0' + month1 : month1;
const year = now.getFullYear();
const nowData = today + "." + month + "." + year;
data.textContent = nowData;
data2.textContent = nowData;
// const createdP=document.createElement("p");
// createdP.textContent=nowData;
// document.body.appendChild(createdP)
const hours = now.getHours() < 10 ? '0' + now.getHours() : now.getHours();
const minutes = now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes();
const nowTime = hours + ":" + minutes;
time.textContent = nowTime;
// createdP.style.color="red";
// const createdP2=document.createElement("p");
// createdP2.textContent=nowTime;
// document.body.appendChild(createdP2);
const inputButton = document.querySelector("#input-button");
const modal = document.querySelector("#modal");
const remove = document.querySelector("#remove");
const close = document.querySelector("#close");
const showModal = document.querySelector("#show-modal")
function added() {
    modal.classList.remove("remove");
    showModal.classList.remove("remove");
}
inputButton.addEventListener("click", added)
function removed() {
    modal.classList.add("remove");
    showModal.classList.add("remove");
}
close.addEventListener("click", removed);
showModal.addEventListener("click", removed);
const addButton = document.querySelector("#add_button");
const todoTask = document.querySelector("#todoTask");
const taskTitle = document.querySelector("#taskTitle");
const titleDescription = document.querySelector("#titleDescription");
//const taskNumber=document.querySelector("#taskNumber");
const dataInput = document.querySelector("#data_input");
//const Description2=document.querySelector("#Description2");
//const data3 = document.querySelector("#data3");
const internalTodo = document.querySelector("#internalTodo");
const todotask = document.querySelector("#todoTask");
// const arrTodo=Array.from(internalTodo.children);
// addButton.addEventListener("click",()=>{
//    todoTask.classList.remove("remove");
//     removed();
//     taskNumber.textContent=taskTitle.value;
//    // data3.textContent=dataInput.value;
//    // Description2.textContent=titleDescription.value;
//     internalTodo.children.appendChild(todotask);
// })
const pen = document.querySelector("#pen");
const taskNumberDefault = document.querySelector("taskNumberDefault");
const Description = document.querySelector("Description");
const changeModal = document.querySelector("changeModal");
const changeButton = document.querySelector("change_button");
const closeTask=document.querySelector("#closeTask");
function changeAdded(){
    changeModal.classList.remove("remove");
    showModal.classList.remove("remove");
}
pen.addEventListener("click",()=>{
    changeAdded();
    taskNumberDefault.textContent = taskTitle.value;
    data2.textContent = dataInput.value;
    Description.textContent = titleDescription.value;
})
closeTask.addEventListener("click",added)
const deleteTask = document.querySelector("#delete");
const todo__taskDefault = document.querySelector("#todo__taskDefault");
deleteTask.addEventListener("click", () => {
    todo__taskDefault.classList.add("remove");
})
/*const newDelete=document.querySelector("#newDelete");
newDelete.addEventListener("click",()=>{
    todoTask.classList.add("remove");
})*/
const close_button=document.querySelector("#close_button");
close_button.addEventListener("click",()=>{
    removed();
})
addButton.addEventListener("click", () => {
    removed();
    const createdDiv = document.createElement("div");
    createdDiv.style.display = "flex";
    createdDiv.style.flexDirection = "column";
    createdDiv.style.gap = "15px";
    createdDiv.style.margin = "20px auto";
    createdDiv.style.width = "80%";
    createdDiv.style.background = "#f1dbfe";
    createdDiv.style.height = "180px";
    createdDiv.style.border = "3px solid rgb(228, 33, 249)";
    createdDiv.style.borderRadius = "5px";
    createdDiv.style.padding = "10px";
    const createdp1 = document.createElement("p");
    const createdp2=document.createElement("p");
    const createdp3=document.querySelector("p");
    const iconDiv=document.querySelector("#icon2");
    createdp1.textContent = taskTitle.value;
    createdp2.textContent = dataInput.value;
    createdp3.textContent = titleDescription.value;
    createdp1.style.color ="rgb(149, 6, 165)";
    createdp1.style.fontSize="20px"
    createdp3.style.fontSize="20px"
    createdp3.style.color ="rgb(149, 6, 165)";
    createdp2.style.color ="#bf77e9";
    createdDiv.appendChild(createdp1);
    createdDiv.appendChild(createdp2);
    createdDiv.appendChild(createdp3);
    createdDiv.appendChild(iconDiv);
    internalTodo.appendChild(createdDiv);
    const pen2=document.querySelector("pen2")
    const deleteTask2=document.querySelector("delete2")
    pen2.classList.remove("remove");
    deleteTask2.classList.remove("remove");
})